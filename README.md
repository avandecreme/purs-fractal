Building a [Heighway dragon](https://en.wikipedia.org/wiki/Dragon_curve#Heighway_dragon)
in PureScript.

![Heighway dragon](dragon.png)

To run:

1. `npm install`
2. `PATH=./node_modules/.bin:$PATH`
3. `spago build --watch`
4. `npm run dev`
