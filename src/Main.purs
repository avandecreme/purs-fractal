module Main where

import Prelude

import Data.Array (concat, fold, tail, take, uncons, zip, (:))
import Data.FunctorWithIndex (mapWithIndex)
import Data.Int (even)
import Data.Maybe (Maybe(..), fromMaybe, maybe)
import Data.Tuple (Tuple(..))
import Effect (Effect)
import Effect.Console as Console
import Web.DOM.Document (Document, createElementNS)
import Web.DOM.Element (Element, setAttribute, toNode)
import Web.DOM.Node (Node, appendChild, setTextContent)
import Web.HTML (window) as HTML
import Web.HTML.HTMLDocument (body, toNonElementParentNode) as HTML
import Web.HTML.HTMLDocument (toDocument)
import Web.HTML.HTMLElement (toElement)
import Web.HTML.Window (document) as HTML

main :: Effect Unit
main = do
  window <- HTML.window
  htmlDocument <- HTML.document window
  let node = HTML.toNonElementParentNode htmlDocument
  body <- HTML.body htmlDocument
  let go = buildPage $ toDocument htmlDocument
  maybe (Console.error "Couldn't find document body") go $ toElement <$> body

buildPage :: Document -> Element -> Effect Unit
buildPage doc body = do
  setTextContent "" $ toNode body -- Warn: removes everything including the script import
  svg <- buildSvg doc
  void $ appendChild svg $ toNode body

buildSvg :: Document -> Effect Node
buildSvg doc = do
  svg <- createSvgElement "svg" doc
  setAttribute "width" "750" svg
  setAttribute "height" "500" svg
  setAttribute "viewBox" "-0.34 -0.34 1.51 1.01" svg
  let svgNode = toNode svg
  addPath doc svgNode $ buildDragon 18
  -- addAxes doc svgNode
  pure svgNode

pointsToLines :: Array Point -> Array (Tuple Point Point)
pointsToLines points = fromMaybe [] $ zip points <$> tail points

buildDragon :: Int -> Array Point
buildDragon level = if level <= 0
  then [{ x: 0.0, y: 0.0 }, { x: 1.0, y: 0.0 }]
  else augmentDragon $ buildDragon (level - 1)

augmentDragon :: Array Point -> Array Point
augmentDragon points =
  let toDir i = if even i then L else R
      mapper i line = foldDragonLine (toDir i) line
      start = take 1 points
  in start <> (concat $ mapWithIndex mapper $ pointsToLines points)

foldDragonLine :: Direction -> Tuple Point Point -> Array Point
foldDragonLine dir (Tuple start end) =
  let xDiff = end.x - start.x
      yDiff = end.y - start.y
      perpVector = case dir of
        L -> { x: -yDiff / 2.0, y:  xDiff / 2.0 }
        R -> { x:  yDiff / 2.0, y: -xDiff / 2.0 }
      center = { x: (start.x + end.x) / 2.0, y: (start.y + end.y) / 2.0 }
      newPoint = { x: center.x + perpVector.x, y: center.y + perpVector.y }
  in [newPoint, end]

data Direction = L | R

type Point =
  { x :: Number
  , y :: Number
  }

addPath :: Document -> Node -> Array Point -> Effect Unit
addPath doc svgNode points = do
  path <- createSvgElement "path" doc
  setAttribute "fill" "none" path
  setAttribute "stroke" "red" path
  setAttribute "stroke-width" "1px" path
  setAttribute "vector-effect" "non-scaling-stroke" path
  setAttribute "d" (toPathCommands points) path
  void $ appendChild (toNode path) svgNode

toPathCommands :: Array Point -> String
toPathCommands points =
  let toString p = show p.x <> "," <> show p.y
      toM p = "M " <> toString p
      toL p = " L " <> toString p
  in case uncons points of
    Nothing             -> ""
    Just { head, tail } -> fold $ (toM head):(map toL tail)

addAxes :: Document -> Node -> Effect Unit
addAxes doc svgNode = do
  xAxe <- createSvgElement "line" doc
  setAttribute "stroke" "black" xAxe
  setAttribute "stroke-width" "0.002" xAxe
  setAttribute "x1" "-10" xAxe
  setAttribute "y1" "0" xAxe
  setAttribute "x2" "10" xAxe
  setAttribute "y2" "0" xAxe
  void $ appendChild (toNode xAxe) svgNode
  yAxe <- createSvgElement "line" doc
  setAttribute "stroke" "black" yAxe
  setAttribute "stroke-width" "0.002" yAxe
  setAttribute "x1" "0" yAxe
  setAttribute "y1" "-10" yAxe
  setAttribute "x2" "0" yAxe
  setAttribute "y2" "10" yAxe
  void $ appendChild (toNode yAxe) svgNode

createSvgElement :: String -> Document -> Effect Element
createSvgElement = createElementNS $ Just "http://www.w3.org/2000/svg"
